<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>PHP & HTML</title>
</head>

<body>
  <h1>Liste des élèves</h1>
  <!-- Instructions : Afficher la liste des éléves qui sont présent dans le tableau $students -->
  <?php
  //students
  $students = ['Hulk', 'Iron Man', 'Wonder Woman', 'Black Widow', 'Malicia'];
  ?>
  <ul>
    <?php foreach ($students as $eleves) {
      echo $eleves . ", ";
    } ?>
  </ul>
  <hr>
  <h1>Date du jour</h1>
  <form>

    <!-- Instructions : Créer la liste de jour (en chiffres), de mois (en chiffres) et d'année en PHP. -->
    <label for="day">Day</label>
    <select name="day"><?php $days = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31];
                        $selected = '';

                        foreach ($days as $daynum) {
                          echo "<option value = " . $daynum . ">$daynum</option>";
                        }

                        ?></select>
    <label for="month">Month</label>
    <select name="month"><?php $months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];

                          foreach ($months as $monthnum) {
                            echo "<option value = " . $monthnum . ">$monthnum</option>";
                          }

                          ?></select>
    <label for="year">Year</label>
    <select name="year"><?php $years = [2019, 2020, 2021, 2022, 2023, 2024, 2025, 2026, 2027, 2028, 2029, 2030];

                        foreach ($years as $yearnum) {
                          echo "<option value=" . $yearnum . ">$yearnum</option>";
                        }

                        ?></select>
  </form>
  <hr>
  <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "fille" -->
  <!-- Instruction : Afficher ce bloc que si dans l'URL il y'a une variable sexe et que ça valeur vaut "garçon" -->
  <!-- Instruction : Afficher ce bloc dans les autres cas -->

  <?php
  if (isset($_GET["sexe"])) {
    if ($_GET['sexe'] == "fille") {
      echo "<p>Je suis une fille</p>";
    } else if ($_GET["sexe"] == "garçon") {
      echo "<p>Je suis un garçon</p>";
    } else {
      echo "<p>Je suis indéfini</p>";
    }
  }
  ?>

</body>

</html>